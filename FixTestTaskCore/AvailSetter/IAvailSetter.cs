﻿using System.Threading.Tasks;

namespace FixTestTaskCore.AvailSetter
{
    public interface IAvailSetter
    {
        Task SetAvailAsync(params Site[] sites);
    }
}