﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace FixTestTaskCore.AvailSetter
{
    public class HttpAvailSetter : IAvailSetter
    {
        private readonly HttpClient _httpClient;

        public HttpAvailSetter(HttpClient httpClient)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task SetAvailAsync(Site[] sites)
        {
            if (sites == null) throw new ArgumentNullException(nameof(sites));

            foreach (var site in sites)
            {
                using (var response = await _httpClient.GetAsync(site.Url))
                {
                    site.Availability = response.StatusCode.ToString();
                }
            }
        }
    }
}