﻿using System;

namespace FixTestTaskCore.AvailSetter
{
    public class TrackData
    {
        public const string DefaultAvail = "Unknown";
        public TrackData(long periodMs)
        {
            Availability = DefaultAvail;
            PeriodMs = periodMs;
        }

        public string Availability { get; set; }

        public DateTime LastTracked { get; set; }

        public long PeriodMs { get; set; }

        public bool IsTrackTime(DateTime dateTime)
        {
            return dateTime >= LastTracked.AddMilliseconds(PeriodMs);
        }
    }
}