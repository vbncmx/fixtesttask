﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Threading;

namespace FixTestTaskCore.AvailSetter
{
    public class StdTrackingAvailSetter : ITrackingAvailSetter
    {
        private readonly IAvailSetter _baseAvailSetter;
        private readonly int _timeoutMs;
        private readonly ConcurrentDictionary<string, TrackData> _trackDictionary = new ConcurrentDictionary<string, TrackData>();
        private readonly EventWaitHandle _stopHandle = new ManualResetEvent(false);
        private Task _task;

        public StdTrackingAvailSetter(IAvailSetter baseAvailSetter, int timeoutMs = 2000)
        {
            _baseAvailSetter = baseAvailSetter ?? throw new ArgumentNullException(nameof(baseAvailSetter));
            _timeoutMs = timeoutMs;
        }

        public Task SetAvailAsync(params Site[] sites)
        {
            if (sites == null) throw new ArgumentNullException(nameof(sites));

            return Task.Run(() =>
            {
                foreach (var site in sites)
                {
                    if (site.Url == null) continue;

                    if (!_trackDictionary.TryGetValue(site.Url, out var trackData)) continue;

                    site.Availability = trackData.Availability;
                }
            });
        }

        public void StartTracking()
        {
            StopTracking();
            _stopHandle.Reset();
            _task = Task.Run(() =>
            {
                while (true)
                {
                    Track();
                    if (_stopHandle.WaitOne(_timeoutMs))
                    {
                        break;
                    }
                }
            });
        }

        private void Track()
        {
            var urls = _trackDictionary.Keys.ToArray();

            var now = DateTime.Now;

            foreach (var url in urls)
            {
                if (!_trackDictionary.TryGetValue(url, out var trackData)) continue;

                if (!trackData.IsTrackTime(now)) continue;

                string avail;

                try
                {
                    var site = new Site(url);
                    _baseAvailSetter.SetAvailAsync(site).Wait();
                    avail = site.Availability;
                }
                catch (Exception ex)
                {
                    avail = $"Error: {ex.Message}";
                }

                trackData.Availability = avail;
                trackData.LastTracked = now;
            }
        }

        public void StopTracking()
        {
            if (_task == null) return;
            _stopHandle.Set();
            _task.Wait();
        }

        public void TrackSites(params Site[] sites)
        {
            if (sites == null) throw new ArgumentNullException(nameof(sites));

            foreach (var site in sites)
            {
                if (site.Url != null)
                {
                    _trackDictionary[site.Url] = new TrackData(site.PeriodMs);
                }
            }
        }

        public void UntrackUrl(string url)
        {
            if (url == null) return;

            _trackDictionary.Remove(url, out var _);
        }
    }
}