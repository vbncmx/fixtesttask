﻿namespace FixTestTaskCore.AvailSetter
{
    public interface ITrackingAvailSetter : IAvailSetter
    {
        void TrackSites(params Site[] sites);
        void UntrackUrl(string url);
    }
}