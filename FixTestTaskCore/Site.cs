﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FixTestTaskCore
{
    public class Site
    {
        public Site()
        { }

        public Site(string url)
        {
            Url = url;
        }

        public Site(string url, int periodMs)
        {
            Url = url;
            PeriodMs = periodMs;
        }

        public int Id { get; set; }

        [Required]
        public string Url { get; set; }

        [Required]
        public long PeriodMs { get; set; }

        public string Availability { get; set; }
    }
}
