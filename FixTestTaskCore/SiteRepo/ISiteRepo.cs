﻿using System.Threading.Tasks;

namespace FixTestTaskCore.SiteRepo
{
    public interface ISiteRepo
    {
        Task<Site> Get(int id);

        Task<Site[]> GetAll();

        Task Add(Site site);

        Task<bool> Remove(int id);

        Task Update(Site site);
    }
}