﻿using System;
using System.Threading.Tasks;
using FixTestTaskCore.AvailSetter;

namespace FixTestTaskCore.SiteRepo
{
    public class AvailUpdatingRepo : ISiteRepo
    {
        private readonly ISiteRepo _baseRepo;
        private readonly ITrackingAvailSetter _trackingAvailSetter;

        public AvailUpdatingRepo(ISiteRepo siteRepo, ITrackingAvailSetter trackingAvailSetter)
        {
            _baseRepo = siteRepo ?? throw new ArgumentNullException(nameof(siteRepo));
            _trackingAvailSetter = trackingAvailSetter ?? throw new ArgumentNullException(nameof(trackingAvailSetter));
        }

        public async Task<Site> Get(int id)
        {
            var site = await _baseRepo.Get(id);
            await _trackingAvailSetter.SetAvailAsync(site);
            return site;
        }

        public async Task<Site[]> GetAll()
        {
            var sites = await _baseRepo.GetAll();
            await _trackingAvailSetter.SetAvailAsync(sites);
            return sites;
        }

        public async Task Add(Site site)
        {
            await _baseRepo.Add(site);
            _trackingAvailSetter.TrackSites(site);
        }

        public async Task<bool> Remove(int id)
        {
            var site = await _baseRepo.Get(id);
            if (site == null)
                return false;
            await _baseRepo.Remove(id);
            _trackingAvailSetter.UntrackUrl(site.Url);
            return true;
        }

        public async Task Update(Site site)
        {
            await _baseRepo.Update(site);
            _trackingAvailSetter.UntrackUrl(site.Url);
            _trackingAvailSetter.TrackSites(site);
        }
    }
}