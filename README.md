### What is this repository for? ###

1. FixTestTaskApi - WebApi
2. FixTestTaskConsole - Консольный клиент для WebApi
3. FixTestTaskCore - Бизнес логика
4. FixTestTaskCoreTest - Юнит тесты для FixTestTaskCore
5. FixTestTaskData - Реализация интерфейса ISiteRepo
6. FixTestTaskIntegTests - Интеграционные тесты для WebApi и JsonFileSiteRepo

### How do I get set up? ###

1. Запускаемые проекты - FixTestTaskApi & FixTestTaskConsole
2. Не забываем указывать baseUrl для FixTestTaskConsole (Debug = в свойствах проекта как первый аргумент комадной строки)

Авторизация:

username: admin
password: 12345678

