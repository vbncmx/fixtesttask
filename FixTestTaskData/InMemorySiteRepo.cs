﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FixTestTaskCore;
using FixTestTaskCore.SiteRepo;

namespace FixTestTaskData
{
    public class InMemorySiteRepo : ISiteRepo
    {
        protected readonly Dictionary<int, Site> _sites = new Dictionary<int, Site>();
        
        public virtual async Task<Site> Get(int id)
        {
            return _sites[id]; 
        }

        public virtual async Task<Site[]> GetAll()
        {
            return _sites.Values.ToArray();
        }

        public virtual async Task Add(Site site)
        {
            site.Id = _sites.Any()
                ? _sites.Keys.Max() + 1
                : 1;

            _sites[site.Id] = site;
        }

        public virtual async Task<bool> Remove(int id)
        {
            if (!_sites.ContainsKey(id)) return false;

            _sites.Remove(id);

            return true;
        }

        public virtual async Task Update(Site site)
        {
            _sites[site.Id] = site;
        }
    }
}
