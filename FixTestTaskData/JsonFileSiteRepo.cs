﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FixTestTaskCore;
using Newtonsoft.Json;

namespace FixTestTaskData
{
    public class JsonFileSiteRepo : InMemorySiteRepo
    {
        private readonly string _fileLocation;
        private bool _isLoaded = false;

        public JsonFileSiteRepo(string fileLocation)
        {
            _fileLocation = fileLocation ?? throw new ArgumentNullException(nameof(fileLocation));
        }

        public override async Task<Site> Get(int id)
        {
            if (!_isLoaded) await Load();

            return await base.Get(id);
        }

        public override async Task<Site[]> GetAll()
        {
            if (!_isLoaded) await Load();

            return await base.GetAll();
        }

        public override async Task Add(Site site)
        {
            if (!_isLoaded) await Load();

            await base.Add(site);

            await Save();
        }

        public override async Task<bool> Remove(int id)
        {
            if (!_isLoaded) await Load();

            var result = await base.Remove(id);

            if (result)
                await Save();

            return result;
        }

        public override async Task Update(Site site)
        {
            if (!_isLoaded) await Load();

            await base.Update(site);

            await Save();
        }

        private async Task Load()
        {
            _sites.Clear();

            if (!File.Exists(_fileLocation))
                return;

            var siteListJson = await File.ReadAllTextAsync(_fileLocation);
            var siteList = JsonConvert.DeserializeObject<List<Site>>(siteListJson);
            foreach (var site in siteList)
            {
                _sites.Add(site.Id, site);
            }
        }

        private async Task Save()
        {
            var siteList = _sites.Values.ToList();
            var siteListJson = JsonConvert.SerializeObject(siteList);
            await File.WriteAllTextAsync(_fileLocation, siteListJson);
        }
    }
}
