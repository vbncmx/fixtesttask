﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FixTestTaskCore;
using FixTestTaskCore.AvailSetter;
using Xunit;

namespace FixTestTaskCoreTests
{
    public class HttpAvailSetterTests
    {
        [Theory]
        [InlineData(HttpStatusCode.OK)]
        [InlineData(HttpStatusCode.BadRequest)]
        [InlineData(HttpStatusCode.NotFound)]
        [InlineData(HttpStatusCode.Forbidden)]
        [InlineData(HttpStatusCode.BadGateway)]
        public async Task SetAvail_UsesStatusCode_From_HttpClientResponse(HttpStatusCode statusCode)
        {
            // ARRANGE

            var httpHandler = new HttpMessageHandlerDummy(statusCode);
            var httpClient = new HttpClient(httpHandler);
            var availSetter = new HttpAvailSetter(httpClient);
            var sites = Enumerable.Range(0, 10).Select(i => new Site($"Hello{i}"){ Url = "http://hello.com" }).ToArray();

            // ACT

            await availSetter.SetAvailAsync(sites);

            foreach (var site in sites)
            {
                Assert.Equal(statusCode.ToString(), site.Availability);
            }
        }

        [Fact]
        public async Task SetAvail_Throws_OnNullInput()
        {
            var httpHandler = new HttpMessageHandlerDummy(HttpStatusCode.OK);
            var httpClient = new HttpClient(httpHandler);
            var availSetter = new HttpAvailSetter(httpClient);

            await Assert.ThrowsAsync<ArgumentNullException>(() => availSetter.SetAvailAsync(null));
        }

        class HttpMessageHandlerDummy : HttpMessageHandler
        {
            private readonly HttpStatusCode _statusCode;

            public HttpMessageHandlerDummy(HttpStatusCode statusCode)
            {
                _statusCode = statusCode;
            }

            protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                var httpResponseMessage = new HttpResponseMessage(_statusCode);
                return Task.FromResult(httpResponseMessage);
            }
        }
    }
}