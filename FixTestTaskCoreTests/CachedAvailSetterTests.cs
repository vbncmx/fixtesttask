using System;
using System.Linq;
using System.Threading.Tasks;
using FixTestTaskCore;
using FixTestTaskCore.AvailSetter;
using Xunit;
using Xunit.Sdk;

namespace FixTestTaskCoreTests
{
    public class StdTrackingAvailSetterTests
    {
        [Fact]
        public async Task TracksSite_After_TrackSitesExecuted()
        {
            // ARRANGE

            var avail = Guid.NewGuid().ToString();
            var baseAvailSetter = new AvailSetterStrategy(ss => ss.ToList().ForEach(s => s.Availability = avail));
            var trackingAvailSetter = new StdTrackingAvailSetter(baseAvailSetter, 5);
            var site = new Site("Hello1", 10);

            // ACT

            trackingAvailSetter.StartTracking();
            trackingAvailSetter.TrackSites(site);
            await Task.Delay(100);
            await trackingAvailSetter.SetAvailAsync(site);
            trackingAvailSetter.StopTracking();

            Assert.Equal(avail, site.Availability);
        }

        [Fact]
        public async Task DoesNotTrackSite_After_UntrackUrlExecuted()
        {
            // ARRANGE

            var avail = Guid.NewGuid().ToString();
            var baseAvailSetter = new AvailSetterStrategy(ss => ss.ToList().ForEach(s => s.Url = Guid.NewGuid().ToString()));
            var trackingAvailSetter = new StdTrackingAvailSetter(baseAvailSetter, 5);
            var site = new Site("Hello1", 10)
            {
                Availability = avail
            };

            // ACT

            trackingAvailSetter.TrackSites(site);
            trackingAvailSetter.StartTracking();
            await Task.Delay(100);
            trackingAvailSetter.UntrackUrl(site.Url);
            await trackingAvailSetter.SetAvailAsync(site);
            trackingAvailSetter.StopTracking();

            Assert.Equal(avail, site.Availability);
        }

        [Fact]
        public async Task SetAvailAsync_Throws_OnNullInput()
        {
            var baseAvailSetter = new AvailSetterStrategy(ss => { });
            var trackingAvailSetter = new StdTrackingAvailSetter(baseAvailSetter, 5);
            await Assert.ThrowsAsync<ArgumentNullException>(() => trackingAvailSetter.SetAvailAsync(null));
        }

        [Fact]
        public void TrackSites_Throws_OnNullInput()
        {
            var baseAvailSetter = new AvailSetterStrategy(ss => { });
            var trackingAvailSetter = new StdTrackingAvailSetter(baseAvailSetter, 5);
            Assert.Throws<ArgumentNullException>(() => trackingAvailSetter.TrackSites(null));
        }
    }

    public class AvailSetterStrategy : IAvailSetter
    {
        private readonly Action<Site[]> _setAvailAction;

        public AvailSetterStrategy(Action<Site[]> setAvailAction)
        {
            _setAvailAction = setAvailAction ?? throw new ArgumentNullException(nameof(setAvailAction));
        }

        public Task SetAvailAsync(params Site[] sites)
        {
            return Task.Run(() => _setAvailAction(sites));
        }
    }
}

