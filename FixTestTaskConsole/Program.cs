﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FixTestTaskCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FixTestTaskConsole
{
    class Program
    {
        private static string _baseUrl;
        private static HttpClient _httpClient = new HttpClient();
        private static string _token = null;

        static void Main(string[] args)
        {
            _baseUrl = args[0].TrimEnd('/') + "/";
            var isBreakRequired = false;
            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                isBreakRequired = true;
                eventArgs.Cancel = true;
            };

            Help();

            Console.WriteLine(">>>>>> ");
            var command = Console.ReadLine();

            while (command != "exit")
            {
                isBreakRequired = false;
                try
                {
                    if (command.StartsWith("add "))
                    {
                        var commandArgs = command.Substring(4).Split(' ');
                        Add(commandArgs[0], int.Parse(commandArgs[1])).Wait();
                    }
                    else if (command.StartsWith("remove "))
                    {
                        var id = int.Parse(command.Substring(7));
                        Remove(id).Wait();
                    }
                    else if (command.StartsWith("update "))
                    {
                        var commandArgs = command.Substring(7).Split(' ');
                        Update(int.Parse(commandArgs[0]), commandArgs[1], int.Parse(commandArgs[2])).Wait();
                    }
                    else if (command.StartsWith("list "))
                    {
                        var sleepSec = int.Parse(command.Substring(5));
                        while (!isBreakRequired)
                        {
                            List().Wait();
                            Thread.Sleep(sleepSec * 1000);
                        }
                    }
                    else if (command.StartsWith("login "))
                    {
                        var commandArgs = command.Substring(6).Split(' ');
                        Login(commandArgs[0], commandArgs[1]).Wait();
                    }
                    else if (command.StartsWith("logout"))
                    {
                        Logout();
                    }
                    else if (command.StartsWith("help"))
                    {
                        Help();
                    }
                    else
                    {
                        Console.WriteLine("Unknown command");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error: {e.Message}");
                }

                Console.WriteLine(">>>>>> ");
                command = Console.ReadLine();
            }
        }

        private static void Logout()
        {
            _token = null;
            Console.WriteLine("<<<<<< ");
            Console.WriteLine($"{DateTime.Now} Logout: Ok");
        }

        private static async Task List()
        {
            Site[] sites;
            using (var response = await _httpClient.GetAsync(_baseUrl + "api/sites/"))
            {
                Console.WriteLine("<<<<<< ");
                Console.WriteLine($"{DateTime.Now} List: {response.StatusCode}");
                var sitesJson = await response.Content.ReadAsStringAsync();
                sites = JsonConvert.DeserializeObject<Site[]>(sitesJson);
            }

            if (sites != null)
            {
                foreach (var site in sites)
                {
                    Console.WriteLine($"{site.Id}. {site.Url} ({site.PeriodMs}) = {site.Availability}");
                }

                if (sites.Length == 0)
                {
                    Console.WriteLine("No site data");
                }
            }
        }

        private static async Task Update(int id, string url, int periodMs)
        {
            var site = new Site(url, periodMs)
            {
                Id = id
            };
            var siteJson = JsonConvert.SerializeObject(site);
            var request = new HttpRequestMessage(HttpMethod.Post, _baseUrl + "api/sites/")
            {
                Content = new StringContent(siteJson, Encoding.UTF8, "application/json")
            };
            if (_token != null)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _token);
            }
            using (var response = await _httpClient.SendAsync(request))
            {
                Console.WriteLine("<<<<<< ");
                Console.WriteLine($"{DateTime.Now} Update: {response.StatusCode}");
            }
        }

        private static async Task Remove(int id)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, _baseUrl + $"api/sites/{id}");
            if (_token != null)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _token);
            }
            using (var response = await _httpClient.SendAsync(request))
            {
                Console.WriteLine("<<<<<< ");
                Console.WriteLine($"{DateTime.Now} Remove: {response.StatusCode}");
            }
        }

        private static async Task Add(string url, int periodMs)
        {
            var site = new Site(url, periodMs);
            var siteJson = JsonConvert.SerializeObject(site);
            var request = new HttpRequestMessage(HttpMethod.Put, _baseUrl + "api/sites/")
            {
                Content = new StringContent(siteJson, Encoding.UTF8, "application/json")
            };
            if (_token != null)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _token);
            }
            using (var response = await _httpClient.SendAsync(request))
            {
                Console.WriteLine("<<<<<< ");
                Console.WriteLine($"{DateTime.Now} Add: {response.StatusCode}");
            }
        }

        private static async Task Login(string username, string password)
        {
            var tokenRequest = new
            {
                Username = username,
                Password = password
            };

            var tokenRequestJson = JsonConvert.SerializeObject(tokenRequest);

            var request = new HttpRequestMessage(HttpMethod.Post, _baseUrl + "api/auth/gettoken")
            {
                Content = new StringContent(tokenRequestJson, Encoding.UTF8, "application/json")
            };

            using (var response = await _httpClient.SendAsync(request))
            {
                Console.WriteLine("<<<<<< ");
                Console.WriteLine($"{DateTime.Now} Authorize: {response.StatusCode}");
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var tokenResponseJson = await response.Content.ReadAsStringAsync();
                    dynamic tokenResponse = JObject.Parse(tokenResponseJson);
                    _token = (string) tokenResponse.token;
                }
            }
        }

        private static void Help()
        {
            Console.WriteLine("List each {x} sec => \tlist {x}, (Ctrl + C) = break");
            Console.WriteLine("Login => \t\tlogin {username} {password}");
            Console.WriteLine("Logout => \t\tlogout");
            Console.WriteLine("Add site => \t\tadd {url} {periodMs}");
            Console.WriteLine("Remove site => \t\tremove {id}");
            Console.WriteLine("Update site => \t\tupdate {id} {url} {periodMs}");
            Console.WriteLine("Exit => \t\texit");
            Console.WriteLine("Help => \t\thelp");
        }
    }
}
