﻿using System;

namespace FixTestTaskApi
{
    public class JwtOptions
    {
        public string SecretKeyB64 { get; set; }

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public byte[] GetSecretKeyBytes()
        {
            return Convert.FromBase64String(SecretKeyB64);
        }
    }
}