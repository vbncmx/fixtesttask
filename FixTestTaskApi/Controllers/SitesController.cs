﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FixTestTaskCore;
using FixTestTaskCore.SiteRepo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace FixTestTaskApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [EnableCors("Welcome")]
    public class SitesController : Controller
    {
        private readonly ISiteRepo _siteRepo;

        public SitesController(ISiteRepo siteRepo)
        {
            _siteRepo = siteRepo ?? throw new ArgumentNullException(nameof(siteRepo));
        }

        // GET api/sites
        [AllowAnonymous]
        [HttpGet]
        public async Task<IEnumerable<Site>> Get()
        {
            return await _siteRepo.GetAll();
        }

        // GET api/sites/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<Site> Get(int id)
        {
            return await  _siteRepo.Get(id);
        }

        // POST api/sites
        [HttpPost]
        public async Task Post([FromBody]Site site)
        {
            await _siteRepo.Update(site);
        }

        // PUT api/sites/5
        [HttpPut]
        public async Task Put([FromBody]Site site)
        {
            await _siteRepo.Add(site);
        }

        // DELETE api/sites/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _siteRepo.Remove(id);
        }
    }
}
