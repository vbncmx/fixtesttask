﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace FixTestTaskApi.Controllers
{
    [Produces("application/json")]
    [Route("api/auth")]
    [EnableCors("Welcome")]
    public class AuthController : Controller
    {
        private readonly JwtOptions _jwtOptions;

        public AuthController(JwtOptions jwtOptions)
        {
            _jwtOptions = jwtOptions ?? throw new ArgumentNullException(nameof(jwtOptions));
        }

        [AllowAnonymous]
        [Route("ping")]
        public IActionResult Ping()
        {
            return Ok(DateTime.Now.ToString());
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("gettoken")]
        public IActionResult RequestToken([FromBody] TokenRequest request)
        {
            if (request.Username != "admin" || request.Password != "12345678")
            {
                return BadRequest("Could not verify username and password");
            }

            var claims = new[]
            {
                new Claim(ClaimTypes.Name, request.Username)
            };

            var key = new SymmetricSecurityKey(_jwtOptions.GetSecretKeyBytes());
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                claims: claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds);

            return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
        }
    }
}