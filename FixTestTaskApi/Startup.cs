﻿using System.IO;
using System.Net.Http;
using System.Reflection;
using FixTestTaskApi.Https;
using FixTestTaskCore.AvailSetter;
using FixTestTaskCore.SiteRepo;
using FixTestTaskData;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Rewrite.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace FixTestTaskApi
{
    public class Startup
    {
        private static readonly HttpClient HttpClient = new HttpClient();

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var siteRepo = ConstructSiteRepo();
            services.AddSingleton(provider => siteRepo);

            var jwtOptions = GetJwtOptions();
            services.AddSingleton(provider => jwtOptions);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = jwtOptions.Issuer,
                        ValidAudience = jwtOptions.Audience,
                        IssuerSigningKey = new SymmetricSecurityKey(jwtOptions.GetSecretKeyBytes())
                    };
                });

            services.AddMvc();

            services.AddCors(o => o.AddPolicy("Welcome", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            // https://blogs.msdn.microsoft.com/webdev/2017/11/29/configuring-https-in-asp-net-core-across-different-platforms/
            int? httpsPort = null;
            var httpsSection = Configuration.GetSection("HttpServer:Endpoints:Https");
            if (httpsSection.Exists())
            {
                var httpsEndpoint = new EndpointConfiguration();
                httpsSection.Bind(httpsEndpoint);
                httpsPort = httpsEndpoint.Port;
            }
            
            app.UseRewriter(new RewriteOptions().AddRedirectToHttps(302, httpsPort));

            app.UseCors("Welcome");
            app.UseMvc();
        }

        protected virtual ISiteRepo ConstructSiteRepo()
        {
            string dataFileLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\data.json";

            var baseRepo = new JsonFileSiteRepo(dataFileLocation);

            var baseAvailSetter = new HttpAvailSetter(HttpClient);

            var trackingAvailSetter = new StdTrackingAvailSetter(baseAvailSetter);

            var availUpdatingRepo = new AvailUpdatingRepo(baseRepo, trackingAvailSetter);

            var sites = baseRepo.GetAll().Result;

            trackingAvailSetter.TrackSites(sites);

            trackingAvailSetter.StartTracking();

            return availUpdatingRepo;
        }

        private static JwtOptions GetJwtOptions()
        {
            return new JwtOptions
            {
                SecretKeyB64 = "0I3BbDj4T7CUcXIGnnC88gVWeBSiR2q/idVip27kZ0RscY58Fe66eloZJZPCc7ZlFvpGH91t71fO2o70I6/Qpw==",
                Issuer = "localhost",
                Audience = "localhost"
            };
        }
    }
}
