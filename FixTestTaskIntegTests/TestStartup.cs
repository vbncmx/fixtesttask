using FixTestTaskApi;
using FixTestTaskCore.SiteRepo;
using Microsoft.Extensions.Configuration;

namespace FixTestTaskIntegTests
{
    public class TestStartup : Startup
    {
        public static ISiteRepo SiteRepo { get; set; }
        public TestStartup(IConfiguration configuration) : base(configuration)
        { }

        protected override ISiteRepo ConstructSiteRepo()
        {
            return SiteRepo;
        }
    }
}