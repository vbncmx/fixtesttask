﻿using System;
using System.IO;
using System.Threading.Tasks;
using FixTestTaskCore;
using FixTestTaskCore.SiteRepo;
using FixTestTaskData;
using Xunit;

namespace FixTestTaskIntegTests
{
    public class JsonFileRepositoryTests
    {
        [Fact]
        public async Task GetAll_Returns_EmptySiteList_IfThereIsNoFileOnDisk()
        {
            var fileLocation = Path.GetTempFileName() + ".json";
            var siteRepo = new JsonFileSiteRepo(fileLocation);
            var sites = await siteRepo.GetAll();

            Assert.Empty(sites);

            if (File.Exists(fileLocation))
            {
                File.Delete(fileLocation);
            }
        }

        [Fact]
        public async Task Add_Creates_File_OnDisk_Witha_GivenSiteData()
        {
            var fileLocation = Path.GetTempFileName() + ".json";
            var siteRepo = new JsonFileSiteRepo(fileLocation);

            var site = new Site(Guid.NewGuid().ToString(), 100000);
            await siteRepo.Add(site);
            
            Assert.True(File.Exists(fileLocation));
            siteRepo = new JsonFileSiteRepo(fileLocation);
            Assert.Single(siteRepo.GetAll().Result);

            if (File.Exists(fileLocation))
            {
                File.Delete(fileLocation);
            }
        }

        // todo: test delete, update
    }
}