using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FixTestTaskCore;
using FixTestTaskData;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Xunit;

namespace FixTestTaskIntegTests
{
    public class WebApiAndDataStorageIntegrTests
    {
        private InMemorySiteRepo _siteRepo;

        public WebApiAndDataStorageIntegrTests()
        {
            _siteRepo = new InMemorySiteRepo();

            for (var i = 0; i < 10; i++)
            {
                var guid = Guid.NewGuid().ToString("N");
                var site = new Site(guid, guid.GetHashCode());
                _siteRepo.Add(site).Wait();
            }

            TestStartup.SiteRepo = _siteRepo;
        }

        [Fact]
        public async Task Get_ReturnsData_IfSiteIs_InRepository()
        {
            var sites = await _siteRepo.GetAll();
            var request = new HttpRequestMessage(HttpMethod.Get, $"/api/sites/{sites[0].Id}");
            var responseString = await GetResponseString(request);

            Assert.Contains(sites[0].Url, responseString, StringComparison.OrdinalIgnoreCase);
            Assert.Contains(sites[0].PeriodMs.ToString(), responseString, StringComparison.OrdinalIgnoreCase);
        }

        [Fact]
        public async Task GetAll_ReturnsData_ForAllSites_InRepository()
        {
            var sites = await _siteRepo.GetAll();
            var request = new HttpRequestMessage(HttpMethod.Get, "/api/sites/");
            var responseString = await GetResponseString(request);
            foreach (var site in sites)
            {
                Assert.Contains(site.Url, responseString, StringComparison.OrdinalIgnoreCase);
                Assert.Contains(site.PeriodMs.ToString(), responseString, StringComparison.OrdinalIgnoreCase);
            }
        }

        [Fact]
        public async Task Put_Creates_NewSite_InRepository()
        {
            var sites = (await _siteRepo.GetAll()).ToArray();
            var message = new HttpRequestMessage(HttpMethod.Put, "/api/sites/");
            var extraSite = new Site(Guid.NewGuid().ToString("N"), 100000);
            message.Content = new StringContent(JsonConvert.SerializeObject(extraSite), Encoding.UTF8, "application/json");

            await GetResponseString(message);

            var updatedSites = (await _siteRepo.GetAll()).ToArray();
            Assert.Equal(1, updatedSites.Length - sites.Length);
            Assert.Equal(1, updatedSites.Count(s => s.Url == extraSite.Url));
        }


        // todo: DELETE, UPDATE


        private async Task<string> GetResponseString(HttpRequestMessage message)
        {
            string responseString;

            using (var server = new TestServer(new WebHostBuilder()
                .UseStartup<TestStartup>()))
            {
                using (var client = server.CreateClient())
                {
                    using (var response = await client.SendAsync(message))
                    {
                        responseString = await response.Content.ReadAsStringAsync();
                    }
                }
            }

            return responseString;
        }
    }
}
